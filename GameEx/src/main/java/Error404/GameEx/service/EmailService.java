package Error404.GameEx.service;

import Error404.GameEx.models.User;
import Error404.GameEx.models.VerificationToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

@Service
public class EmailService {
    private final VerificationTokenService verificationTokenService;
    public final TemplateEngine templateEngine;
    private final JavaMailSender javaMailSender;
    @Autowired
    public EmailService(VerificationTokenService verificationTokenService, TemplateEngine templateEngine,
                        JavaMailSender javaMailSender){
        this.verificationTokenService=verificationTokenService;
        this.templateEngine=templateEngine;
        this.javaMailSender=javaMailSender;
    }

    public void sendHtmlMail(User user) throws MessagingException{
        VerificationToken verificationToken=verificationTokenService.findByUser(user);
        if(verificationToken!=null){
            String token=verificationToken.getToken();
            Context context = new Context();
            context.setVariable("title","Verify your email address");
            context.setVariable("link","http://localhost:8080/activation?token="+token);
            // create an HTML template and pass the variable to it
            String body=templateEngine.process("verification",context);

            // Send the verification email
            MimeMessage message=javaMailSender.createMimeMessage();
            MimeMessageHelper helper=new MimeMessageHelper(message,true);
            helper.setTo(user.getEmail());
            helper.setSubject("email address verification");
            helper.setText(body,true);
            javaMailSender.send(message);


        }
    }
}
