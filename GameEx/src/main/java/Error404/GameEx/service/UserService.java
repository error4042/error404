package Error404.GameEx.service;


import Error404.GameEx.models.User;
import Error404.GameEx.web.dto.UserRegistrationDto;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends UserDetailsService {
    User save(UserRegistrationDto registrationDto);

}
