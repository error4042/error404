package Error404.GameEx.service;

import Error404.GameEx.models.Role;
import Error404.GameEx.models.User;
import Error404.GameEx.models.VerificationToken;
import Error404.GameEx.repository.RoleRepository;
import Error404.GameEx.repository.UserRepository;
import Error404.GameEx.web.dto.UserRegistrationDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Collection;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService{
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private BCryptPasswordEncoder passwordEncoder;
    @Autowired
    private RoleRepository roleRepository;

    private final VerificationTokenService verificationTokenService;

    @Lazy
    @Autowired
    private EmailService emailService;

    public UserServiceImpl(UserRepository userRepository,VerificationTokenService verificationTokenService,RoleRepository roleRepository) {
        this.userRepository = userRepository;
        this.verificationTokenService=verificationTokenService;
        this.roleRepository=roleRepository;

    }

    @Override
    public User save(UserRegistrationDto registrationDto){
        User user;
        if(userRepository.findAll().isEmpty()) {
            user = new User(registrationDto.getFirstName(), registrationDto.getLastName(), registrationDto.getEmail(), registrationDto.getProfilePicture(), passwordEncoder.encode(registrationDto.getPassword()), Arrays.asList(roleRepository.findRoleByName("ROLE_ADMIN")),
                    null);
        }else {
            user = new User(registrationDto.getFirstName(), registrationDto.getLastName(), registrationDto.getEmail(), registrationDto.getProfilePicture(), passwordEncoder.encode(registrationDto.getPassword()), Arrays.asList(roleRepository.findRoleByName("ROLE_IGRAC")),
                    null);
        }
        if(registrationDto.getCompanyName()!=null){
            user.setTelephone(registrationDto.getTelephone());
            user.setCompanyName(registrationDto.getCompanyName());
            user.setWebAddress(registrationDto.getWebAddress());
            user.setAddress(registrationDto.getAddress());
            user.setOIB(registrationDto.getOIB());
        }
        user.setEnabled(false);
        Optional<User> saved=Optional.of(userRepository.save(user));

        saved.ifPresent(u-> {
            try{
                String token= UUID.randomUUID().toString();
                verificationTokenService.save(saved.get(),token);

                //send verification email
                emailService.sendHtmlMail(u);
            }catch(Exception e){
                e.printStackTrace();
            }
        });
        return  saved.get();
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException{
        User user=userRepository.findByEmail(username);
        if(user==null || !user.isEnabled()){
            throw new UsernameNotFoundException("Invalid username or password.");
        }

        return new org.springframework.security.core.userdetails.User(user.getEmail(),user.getPassword(),mapRolesToAuthorities(user.getRoles()));

    }
    private Collection<? extends GrantedAuthority> mapRolesToAuthorities(Collection<Role> roles){
        return roles.stream().map(role -> new SimpleGrantedAuthority(role.getName())).collect(Collectors.toList());

    }

}
