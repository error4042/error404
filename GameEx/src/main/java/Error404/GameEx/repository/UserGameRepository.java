package Error404.GameEx.repository;

import java.util.List;
import java.util.Set;

import Error404.GameEx.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import Error404.GameEx.models.Game;
import Error404.GameEx.models.UserGame;
import Error404.GameEx.models.UserGameId;


public interface UserGameRepository extends JpaRepository<UserGame,Long> {

	 @Query("select u.gameId from UserGame u where u.userId.id = :user_id")
	    Set<Game> findInventoryByUserId(Long user_id);
	 @Query("select u from UserGame u where u.gameId.game_id = :game_id and u.userId.id = :user_id")
	  UserGame findInventoryByGameId(Long game_id, Long user_id);
	 @Query("select u from UserGame u where u.gameId.game_id=:game_id")
	 List<UserGame> findUserGameByGameId(Long game_id);
	 @Query("select u.userId.email from UserGame u where u.cijena = 0 and u.zaIznajmljivanje = true")
	 Set<String> findPlayersWhoRentGames();
	@Query("select u.userId.email from UserGame u where u.zaIznajmljivanje = true")
	Set<String> findCompanyWhoRentGames();
	@Query("select u from UserGame u where u.userId.id = :user_id")
	Set<UserGame> findUserGameInventory(Long user_id);
}
