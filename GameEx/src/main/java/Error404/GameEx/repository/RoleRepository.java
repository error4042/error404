package Error404.GameEx.repository;

import Error404.GameEx.models.Game;
import Error404.GameEx.models.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface RoleRepository extends JpaRepository<Role,Long> {
    @Query("select r from Role r where r.name = :name")
    Role findRoleByName(String name);

}
