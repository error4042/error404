package Error404.GameEx.repository;

import Error404.GameEx.models.Forum;
import Error404.GameEx.models.Message;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MessageRepository extends JpaRepository<Message,Long> {
    @Query(value="select * from message where user_id=:user_id",nativeQuery = true)
    List<Message> findMessagesFromUser(Long user_id);
    @Query(value="select * from message where forum_id=:forum_id",nativeQuery = true)
    List<Message> findMessagesFromGroup(Long forum_id);
}