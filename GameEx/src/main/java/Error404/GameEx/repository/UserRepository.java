package Error404.GameEx.repository;

import Error404.GameEx.models.Game;
import Error404.GameEx.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface UserRepository extends JpaRepository<User,Long> {
    User findByEmail(String email);

    @Query("select u.inventory from User u join u.inventory where u.id = :user_id")
    Set<Game> findInventoryByUserId(Long user_id);
}
