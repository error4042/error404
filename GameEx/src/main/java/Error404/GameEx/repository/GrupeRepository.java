package Error404.GameEx.repository;

import Error404.GameEx.models.Grupe;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GrupeRepository extends JpaRepository<Grupe,Long> {
}
