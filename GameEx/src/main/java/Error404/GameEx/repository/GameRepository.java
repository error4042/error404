package Error404.GameEx.repository;

import Error404.GameEx.models.Game;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public interface GameRepository extends JpaRepository<Game, Long> {
}
