package Error404.GameEx.repository;

import Error404.GameEx.models.Forum;
import Error404.GameEx.models.Game;
import Error404.GameEx.models.Grupe;
import Error404.GameEx.models.Message;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface ForumRepository extends JpaRepository<Forum,Long> {

    @Query("select f from Forum f where f.grupa.id=:grupa_id")
    Forum findForumByGroupId(long grupa_id);
}
