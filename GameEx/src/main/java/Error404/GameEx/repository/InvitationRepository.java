package Error404.GameEx.repository;

import Error404.GameEx.models.Invitation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface InvitationRepository extends JpaRepository<Invitation,Long> {
        @Query(value="select * from invitation where user_id=:user_id",nativeQuery = true)
        List<Invitation> findInvitationsByUser(Long user_id);


}
