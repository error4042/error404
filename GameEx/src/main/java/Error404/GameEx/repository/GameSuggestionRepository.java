package Error404.GameEx.repository;

import Error404.GameEx.models.GameSuggestion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GameSuggestionRepository extends JpaRepository<GameSuggestion,Long> {
}
