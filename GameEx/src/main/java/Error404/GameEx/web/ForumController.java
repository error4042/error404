package Error404.GameEx.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ForumController {

    @GetMapping("/grupe/{*grupid}/forum")
    public String grupaUsers() {
        return "Forum/forum";
    }
    @GetMapping("/grupe/{*grupid}/editForum")
    public String editForum() {
        return "Forum/editForum";
    }

    @GetMapping("/grupe/{*grupid}/editForum/edit/{*id}")
    public String editMessage() {
        return "Forum/editMessage";
    }
}
