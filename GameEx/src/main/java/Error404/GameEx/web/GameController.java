package Error404.GameEx.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@Controller
public class GameController {

    @GetMapping("/gameindex")
    public String gameindex(){
        return "Game/gameindex";
    }
    @GetMapping("/gamelist")
    public String gamelist(){
        return "Game/gamelist";
    }

    @GetMapping("/gameadd")
    public String gameadd(){
        return "Game/gameadd";
    }

    @GetMapping("/gamesuggest")
    public String gamesuggest(){
        return "Game/gamesuggest";
    }

    @GetMapping("/gamelist/gameadd")
    public String addGame(){ return "Game/gameadd"; }

    @GetMapping("/myinventory")
    public String inventory(){ return "Game/myinventory"; }
    @GetMapping("/gameindex/myinventory/addGame")
    public String addToInventory(){
        return "Game/addGameToInventory";
    }
    @GetMapping("/gamesuggest/list")
    public String gamesuggestlist(){
        return "Game/gamesuggestlist";
    }

    @GetMapping("/rentGamesPlayerOnly")
    public String rentGamesPlayer() {
        return "Game/rentGamesPlayerOnly";
    }
    @GetMapping("/rentGames/Player/{*playerid}")
    public String rentGamesFromPlayers(){   return "Game/rentGamesFromPlayer";}
    @GetMapping("/rentGames/Company/{*companyid}")
    public String rentGamesFromCompany(){   return "Game/rentGamesFromCompany";}
    @GetMapping("/rentGamesCompanyOnly")
    public String rentGamesCompany() {
        return "Game/rentGamesCompanyOnly";
    }
}
