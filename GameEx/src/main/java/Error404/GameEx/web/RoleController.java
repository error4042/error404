package Error404.GameEx.web;

import Error404.GameEx.models.Role;
import Error404.GameEx.repository.RoleRepository;
import Error404.GameEx.web.dto.UserRegistrationDto;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Optional;

@Controller
public class RoleController {
    private final RoleRepository roleRepository;
    public RoleController(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    @GetMapping("/admin/roles")
    public String roles() {
        return "Role/roles";
    }
    @GetMapping("/admin/roles/edit/**")
    public String edit(){
        return "Role/editRoles";
    }
    @GetMapping("/admin/roles/create")
    public String create(){
        return "Role/createRoles";
    }


}
