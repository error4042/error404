package Error404.GameEx.web;

import Error404.GameEx.models.FileUploadUtil;
import Error404.GameEx.models.User;
import Error404.GameEx.models.VerificationToken;
import Error404.GameEx.service.UserService;
import Error404.GameEx.service.VerificationTokenService;
import Error404.GameEx.web.dto.UserRegistrationDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.sql.Timestamp;

@Controller
@RequestMapping("/registration")
public class UserRegistrationController {
    private UserService userService;
    private final VerificationTokenService verificationTokenService;
    @Autowired
    public UserRegistrationController(UserService userService, VerificationTokenService verificationTokenService) {
        this.userService = userService;
        this.verificationTokenService=verificationTokenService;
    }
    @GetMapping
    public String showRegistrationForm(){
        return "registration";
    }

    @ModelAttribute("user")
    public UserRegistrationDto userRegistrationDto(){

        return new UserRegistrationDto();
    }

    @PostMapping
    public String registerUserAccount(@ModelAttribute("user")UserRegistrationDto registrationDto,@RequestParam("image") MultipartFile multipartFile) throws IOException {
        String fileName= StringUtils.cleanPath(multipartFile.getOriginalFilename());
        registrationDto.setProfilePicture(fileName);
        System.out.println(fileName);
        User  savedUser=userService.save(registrationDto);
        String uploadDir = "user-photos/" + savedUser.getId();

        FileUploadUtil.saveFile(uploadDir, fileName, multipartFile);

        return "redirect:/registration?success";

    }


}
