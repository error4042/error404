package Error404.GameEx.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class GrupaController {


    @GetMapping("/grupe")
    public String grupe(){
        return "Grupe/grupe";
    }
    @GetMapping("/grupe/createGrupa")
    public String createGrupe(){
        return "Grupe/createGrupa";
    }
    @GetMapping("/grupe/inGrupa")
    public String inGrupa() {
        return "Grupe/inGrupa";
    }

    @GetMapping("/grupe/allGrupe")
    public String allGrupe() {
        return "Grupe/allGrupe";
    }

    @GetMapping("/grupe/grupaUsers/{*grupid}")
    public String grupaUsers() {
        return "Grupe/grupaUsers";
    }

    @GetMapping("/grupe/{*grupid}/dateSuggestion")
    public String grupaDateSuggestion() {
        return "Grupe/dateSuggestion";
    }
}
