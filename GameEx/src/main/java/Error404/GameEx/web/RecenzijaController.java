package Error404.GameEx.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class RecenzijaController {
    @GetMapping("recenzija")
    public String recenzija() {
        return "Recenzija/recenzije";
    }

    @GetMapping("createRecenzija/user/{*recenzijaid}")
    public String userRecenzija() {
        return "Recenzija/createRecenzija";
    }
}
