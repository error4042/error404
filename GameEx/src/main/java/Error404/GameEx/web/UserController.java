package Error404.GameEx.web;

import Error404.GameEx.models.User;
import Error404.GameEx.models.VerificationToken;
import Error404.GameEx.repository.UserRepository;
import Error404.GameEx.service.UserService;
import Error404.GameEx.service.VerificationTokenService;
import Error404.GameEx.web.dto.UserRegistrationDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.sql.Timestamp;

@Controller
public class UserController {
    private UserService userService;
    private final VerificationTokenService verificationTokenService;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    public UserController(UserService userService, VerificationTokenService verificationTokenService,UserRepository userRepository) {
        this.userService = userService;
        this.userRepository=userRepository;
        this.verificationTokenService=verificationTokenService;
    }
    @GetMapping("/admin/users")
    public String users() {
        return "users";
    }

    @GetMapping("/admin/users/edit/**")
    public String edit() {return "editUsers";}

    @GetMapping("/activation")
    public String activation(@RequestParam("token") String token, Model model){
        VerificationToken verificationToken=verificationTokenService.findByToken(token);
        System.out.println("verification token");
        if(verificationToken==null){
            model.addAttribute("message","Your verification token is invalid.");
        }else{

            User user=verificationToken.getUser();
            //if the user account is not activated
            if(!user.isEnabled()){
                // get the current timestamp
                Timestamp currentTimestamp=new Timestamp(System.currentTimeMillis());
                //check if the token is expired
                if(verificationToken.getExpiryDate().before(currentTimestamp)){
                    model.addAttribute("message","Your verification token has expired");
                }else{
                    //the token is valid
                    //activate the user account
                    user.setEnabled(true);
                    userRepository.save(user);
                    model.addAttribute("message","Your account is succesfully activated");
                }
            }else{
                //the user account is already activated
                model.addAttribute("message","your account is already activated");
            }
        }
        // add '/activation' to SecurityConfig
        return "activation";
    }
}
