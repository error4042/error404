package Error404.GameEx.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class AdController {

    @GetMapping("/ad/createAd")
    public String createAd(){
        return "Ad/createAd";
    }
    @GetMapping("/ad")
    public String indexAd(){
        return "Ad/adsIndex";
    }
    @GetMapping("/ad/adLocation/{*adid}")
    public String adLocation() {
        return "Ad/adLocation";
    }
}
