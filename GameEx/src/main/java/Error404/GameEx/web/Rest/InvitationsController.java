package Error404.GameEx.web.Rest;

import Error404.GameEx.models.Grupe;
import Error404.GameEx.models.Invitation;
import Error404.GameEx.models.User;
import Error404.GameEx.repository.GrupeRepository;
import Error404.GameEx.repository.InvitationRepository;
import Error404.GameEx.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/Invitation")
public class InvitationsController {
    @Autowired
    private GrupeRepository grupeRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private InvitationRepository invitationRepository;
    @GetMapping("/getAllUsersExceptForGroup/{id}")
    public List<User> getAllWithoutGroupUsers(@PathVariable("id") long group_id){
        Grupe grupa=grupeRepository.findById(group_id).orElse(null);
        List<User> allUsers=userRepository.findAll();
        allUsers.removeAll(grupa.getUsers());
        List<User> iznajmljivaci=allUsers.stream().filter(user -> user.isIznajmljivac()).collect(Collectors.toList());
        allUsers.remove(iznajmljivaci);
        return allUsers;

    }
    @GetMapping("/getEmptyInvitation")
    public Invitation getEmptyInvitation(){
        return new Invitation();
    }
    @PostMapping("/createInvitation/")
    public void createInvitation(@RequestBody Invitation invitation){
        Invitation newInvitation= new Invitation();
        newInvitation.setDescription(invitation.getDescription());
        newInvitation.setGrupa_id(invitation.getGrupa_id());
        newInvitation.setUser_id(invitation.getUser_id());
        newInvitation.setGroupName(grupeRepository.findById(invitation.getGrupa_id()).orElse(null).getName());
        invitationRepository.save(newInvitation);
    }
    @GetMapping("/getInvitations")
    public List<Invitation> getInvitationsForUser(){
        Authentication loggedInUser = SecurityContextHolder.getContext().getAuthentication();
        String email = loggedInUser.getName();
        User user=userRepository.findByEmail(email);
        return invitationRepository.findInvitationsByUser(user.getId());

    }
    @GetMapping("/getGroupName/{id}")
    public String getGroupName(@PathVariable("id") long group_id){
        return grupeRepository.findById(group_id).orElse(null).getName();

    }
    @PostMapping("/acceptInvitation")
    public void acceptInvitation(@RequestBody Invitation invitation){
        Authentication loggedInUser = SecurityContextHolder.getContext().getAuthentication();
        String email = loggedInUser.getName();
        User user=userRepository.findByEmail(email);
        Grupe grupe=grupeRepository.findById(invitation.getGrupa_id()).orElse(null);
        grupe.addUser(user);
        grupeRepository.save(grupe);
        invitationRepository.deleteById(invitation.getInvitation_id());
    }





}
