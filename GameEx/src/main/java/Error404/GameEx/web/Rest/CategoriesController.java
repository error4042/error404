package Error404.GameEx.web.Rest;

import Error404.GameEx.models.Category;
import Error404.GameEx.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/categories")
public class CategoriesController {

    @Autowired
    private CategoryRepository categoryRepository;

    @GetMapping("/getCategories")
    public List<Category> list() {
        return categoryRepository.findAll();
    }
}
