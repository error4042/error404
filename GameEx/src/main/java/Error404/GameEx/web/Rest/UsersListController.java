package Error404.GameEx.web.Rest;

import Error404.GameEx.models.User;
import Error404.GameEx.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/userslist")
public class UsersListController {

    @Autowired
    private UserRepository userRepository;

    @GetMapping("/getUsers")
    public List<User> list() { return userRepository.findAll();
    }
    @GetMapping("/getPicturePath")
    public String photoPath(){
        Authentication loggedInUser = SecurityContextHolder.getContext().getAuthentication();
        String email = loggedInUser.getName();
        User user = userRepository.findByEmail(email);
        return  user.getPhotosImagePath();
    }

}