package Error404.GameEx.web.Rest;

import Error404.GameEx.models.Forum;
import Error404.GameEx.models.Message;
import Error404.GameEx.models.User;
import Error404.GameEx.repository.ForumRepository;
import Error404.GameEx.repository.GrupeRepository;
import Error404.GameEx.repository.MessageRepository;
import Error404.GameEx.repository.UserRepository;
import Error404.GameEx.service.UserService;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.net.http.HttpResponse;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
public class ForumsController {
    private final UserRepository userRepository;
    private final ForumRepository forumRepository;
    private final MessageRepository messageRepository;
    public ForumsController( UserRepository userRepository,ForumRepository forumRepository,MessageRepository messageRepository) {

        this.userRepository = userRepository;

        this.forumRepository = forumRepository;
        this.messageRepository=messageRepository;
    }

    @GetMapping("grupe/{id}/forum/getMessages")
    public List<Message> getMessages(@PathVariable("id") long id){
        Long probaId=id;
        return forumRepository.findForumByGroupId(id).getMessages();
    }
    @GetMapping("grupe/{id}/forum/getEmptyMessage")
    public Message getEmptyMessage(){
        Message message= new Message();
        return message;
    }
    @GetMapping("grupe/{id}/editForum/getMessages")
    public List<Message> editMessages(@PathVariable("id") long id){
        Long probaId=id;
        return forumRepository.findForumByGroupId(id).getMessages();
    }
    @GetMapping("grupe/{id}/editForum/getEmptyMessage")
    public Message editMessage(){
        Message message= new Message();
        return message;
    }
    @GetMapping("grupe/{id}/editForum/edit/{mess_id}/getMessage")
    public Message getMessage(@PathVariable("mess_id") long id) {

        return messageRepository.findById(id).orElse(null);
    }
    @PostMapping("grupe/{id}/forum/PostMessage")
    public void postMessage(@RequestBody Message message,@PathVariable("id") long id){
        Authentication loggedInUser = SecurityContextHolder.getContext().getAuthentication();
        String email = loggedInUser.getName();
        User user = userRepository.findByEmail(email);
        message.setUser(user);
        message.setForum(forumRepository.findForumByGroupId(id));
        messageRepository.save(message);
    }
    @PostMapping("/removeMessage")
    public void removeMessage(@RequestBody Message message){
        messageRepository.delete(message);
    }
    @PostMapping("/setMessage")
    public void setUser(@RequestBody Message message){
        Message oldMessage = messageRepository.findById(message.getId()).orElse(null);
        oldMessage.setUser(message.getUser());
        oldMessage.setText(message.getText());
        messageRepository.save(oldMessage);

    }

}
