package Error404.GameEx.web.Rest;

import Error404.GameEx.models.Ad;
import Error404.GameEx.models.Game;
import Error404.GameEx.models.User;
import Error404.GameEx.models.UserGame;
import Error404.GameEx.repository.AdRepository;
import Error404.GameEx.repository.GameRepository;
import Error404.GameEx.repository.UserGameRepository;
import Error404.GameEx.repository.UserRepository;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/ad")
public class AdsController {

    private final AdRepository adRepository;
    private final GameRepository gameRepository;
    private final UserRepository userRepository;
    private final UserGameRepository userGameRepository;
    public AdsController(AdRepository adRepository, GameRepository gameRepository, UserRepository userRepository,UserGameRepository userGameRepository) {
        this.adRepository = adRepository;
        this.gameRepository = gameRepository;
        this.userRepository = userRepository;
        this.userGameRepository=userGameRepository;

    }

    @GetMapping("/getAds")
    public List<Ad> listAd() {
        return adRepository.findAll();
    }

    @PostMapping("/createAd")
    public void createAd(@RequestBody Ad ad) {
        adRepository.save(ad);
    }

    @GetMapping("/getEmptyAd")
    public Ad emptyAd() {
        return new Ad();
    }

    @GetMapping("/getGames")
    public List<Game> listGames() {
        Authentication loggedInUser = SecurityContextHolder.getContext().getAuthentication();
        String name = loggedInUser.getName();
        User user=userRepository.findByEmail(name);
        return userGameRepository.findInventoryByUserId(user.getId()).stream().collect(Collectors.toList());
    }

    @GetMapping("/getUser")
    public User getUser() {
        Authentication loggedInUser = SecurityContextHolder.getContext().getAuthentication();
        String name = loggedInUser.getName();
        return userRepository.findByEmail(name);
    }

    @GetMapping("/adLocationn/{id}")
    public String getLocation(@PathVariable("id") long id) {
        return adRepository.findById(id).orElse(null).getLocation();
    }

    @PostMapping("/deleteAd")
    public void deleteAd(@RequestBody Ad ad){
        adRepository.deleteById(ad.getAd_id());
    }

    @PostMapping("/createad")
    public void createad(@RequestBody Ad ad, @RequestParam("lat") String lat, @RequestParam("lng") String lng) {

        String location = lat + "/" + lng;
        ad.setLocation(location);
        adRepository.save(ad);
    }
}
