package Error404.GameEx.web.Rest;

import Error404.GameEx.models.Forum;
import Error404.GameEx.models.Game;
import Error404.GameEx.models.Grupe;
import Error404.GameEx.models.User;
import Error404.GameEx.repository.*;

import Error404.GameEx.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/grupe")
public class GrupeController {
    private final GrupeRepository grupeRepository;
    private final UserRepository userRepository;
    private final UserService userService;
    private final ForumRepository forumRepository;
    private final UserGameRepository userGameRepository;
    @Autowired
    private MessageRepository messageRepository;
    public GrupeController(GrupeRepository grupeRepository, UserRepository userRepository, UserService userService, ForumRepository forumRepository,UserGameRepository userGameRepository) {
        this.grupeRepository = grupeRepository;
        this.userRepository = userRepository;
        this.userService=userService;
        this.forumRepository = forumRepository;
        this.userGameRepository=userGameRepository;
    }

    @GetMapping("/getUsers")
    public List<User> listUser() {
        Authentication loggedInUser = SecurityContextHolder.getContext().getAuthentication();
        String email = loggedInUser.getName();
        User user = userRepository.findByEmail(email);
        List<User> allusers=userRepository.findAll();
        allusers.remove(user);
        List<User> iznajmljivaci=allusers.stream().filter(user1 -> user1.isIznajmljivac()).collect(Collectors.toList());
        allusers.remove(iznajmljivaci);
        return allusers;
    }

    @GetMapping("/getAuthUser/{username}")
    public Optional<User> getAuthUser(@PathVariable String username){
        User user = userRepository.findByEmail(username);
        return userRepository.findById(user.getId());
    }

    @GetMapping("/getInventory")
    public Set<Game> listGame(){
        Authentication loggedInUser = SecurityContextHolder.getContext().getAuthentication();
        String email = loggedInUser.getName();
        User user= userRepository.findByEmail(email);
        return userGameRepository.findInventoryByUserId(user.getId());

    }
    @GetMapping("/create/getCurrentUser")
    public User getCurrentUser(){
        Authentication loggedInUser = SecurityContextHolder.getContext().getAuthentication();
        String email = loggedInUser.getName();
        User user = userRepository.findByEmail(email);
        return user;
    }
    @GetMapping("/grupaUserss/{id}")
    public Set<User> getGroup(@PathVariable("id") long id){
        Collection<User> usersCol=grupeRepository.findById(id).orElse(null).getUsers();
        Set<User> users=usersCol.stream().collect(Collectors.toSet());
        return  users;
    }

    @GetMapping("/inGrupa/getGrupe")
    public List<Grupe> listfiltered() {
        Authentication loggedInUser = SecurityContextHolder.getContext().getAuthentication();
        String email = loggedInUser.getName();
        User user = userRepository.findByEmail(email);

        return grupeRepository.findAll()
                .stream()
                .filter(g -> g.getUsers().contains(user))
                .collect(Collectors.toList());
    }
    @GetMapping("/allGrupe/getGrupe")
    public List<Grupe> listgrupe() {

        return grupeRepository.findAll()
                .stream()
                .collect(Collectors.toList());
    }
    @GetMapping("/getGrupe")
    public List<Grupe> list() {
        return grupeRepository.findAll();
    }
    @GetMapping("/create/getNewGrupa")
    public Grupe getGrupa(){
        return new Grupe();
    }
    @GetMapping("/getGrupa/{id}")
    public Optional<Grupe> getGrupa(@PathVariable("id") long id){
        return grupeRepository.findById(id);
    }
    @PostMapping("/create/createNewGrupa")
    public void createNewGrupa(@RequestBody Grupe grupa){
        ArrayList<User> users=new ArrayList<>();
        users.add(userRepository.findById(grupa.getGroupAdminId()).orElse(null));
        Grupe newGrupa=new Grupe(grupa.getName(),grupa.getGroupAdminId(),users,grupa.getGame_name());
        grupeRepository.save(newGrupa);
        Forum forum=new Forum(newGrupa);
        forumRepository.save(forum);
    }
    @PostMapping("/create/createNewGrupaWithUsers")
    public void createNewGrupaWithUsers(@RequestBody Grupe grupa){
        int i;
        ArrayList<User> addusers=new ArrayList<>();
        List<User> users=grupa.getUsers().stream().collect(Collectors.toList());
        for(i=0;i<users.size();i++){
            addusers.add(userRepository.findById(users.get(i).getId()).orElse(null));

        }

        addusers.add(userRepository.findById(grupa.getGroupAdminId()).orElse(null));
        Grupe newGrupa=new Grupe(grupa.getName(),grupa.getGroupAdminId(),addusers,grupa.getGame_name());
        System.out.print(addusers);
        grupeRepository.save(newGrupa);
        Forum forum=new Forum(newGrupa);
        forumRepository.save(forum);

    }

    //dodano
    @PostMapping("/setGrupaDate")
    public void setGrupaDate(@RequestBody Grupe grupa){
        Grupe oldGrupa = grupeRepository.findById(grupa.getId()).orElse(null);
        oldGrupa.setDateSuggestion(grupa.getDateSuggestion());

        grupeRepository.save(oldGrupa);
    }
    @PostMapping("/exitGrupa")
    public void exitGroup(@RequestBody Grupe grupa){
        if(grupa.getUsers().stream().collect(Collectors.toList()).size()==1){
            deleteGroup(grupa);
        }else {
            Authentication loggedInUser = SecurityContextHolder.getContext().getAuthentication();
            String email = loggedInUser.getName();
            User user = userRepository.findByEmail(email);
            Grupe istaGrupa = grupeRepository.findById(grupa.getId()).orElse(null);
            if (user.getId() == grupa.getGroupAdminId()) {
                List<User> users = grupa.getUsers().stream().collect(Collectors.toList());
                istaGrupa.setGroupAdminId(users.get(1).getId());
            }
            messageRepository.findMessagesFromUser(user.getId()).forEach(message -> message.setUser(null));
            istaGrupa.removeUser(user);
            grupeRepository.save(istaGrupa);
        }
    }
    @PostMapping("/removePlayerFromGroup/{group_id}")
    public void removePlayerFromGroup(@RequestBody User user,@PathVariable("group_id") long group_id){
        Grupe istaGrupa = grupeRepository.findById(group_id).orElse(null);
        messageRepository.findMessagesFromUser(user.getId()).forEach(message -> message.setUser(null));
        istaGrupa.removeUser(user);
        grupeRepository.save(istaGrupa);

    }
    @PostMapping("/deleteGroup")
    public void deleteGroup(@RequestBody Grupe grupa){
        Forum forum=forumRepository.findForumByGroupId(grupa.getId());
        messageRepository.findMessagesFromGroup(grupa.getId()).forEach(message -> messageRepository.delete(message));
        forumRepository.delete(forum);
        grupeRepository.delete(grupa);
    }
}
