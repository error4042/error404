package Error404.GameEx.web.Rest;

import Error404.GameEx.models.*;
import Error404.GameEx.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/gameindex")
public class GamesController {

    @Autowired
    private GameRepository gameRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserGameRepository userGameRepository;
    @Autowired
    private GameSuggestionRepository gameSuggestionRepository;

    @Autowired
    private AdRepository adRepository;
    @GetMapping("/gamelist/getGames")
    public List<Game> listSuggestion() {
        return gameRepository.findAll();
    }

    @PostMapping("/gamelist/removeGame")
    public void removeGame(@RequestBody Game game){
        List<UserGame> deletegamelist=userGameRepository.findUserGameByGameId(game.getId());
        deletegamelist.forEach(userGame -> userGameRepository.delete(userGame));
        List<Ad> adList=adRepository.findAll().stream().filter(ad -> ad.getGame().getId().equals(game.getId())).collect(Collectors.toList());
        adList.forEach(ad -> adRepository.delete(ad));
        gameRepository.deleteById(game.getId());

    }
    @PostMapping("/myinventory/removeGame")
    public void removeGameFromInventory(@RequestBody Game game){
        List<UserGame> deletegamelist=userGameRepository.findUserGameByGameId(game.getId());
        deletegamelist.forEach(userGame -> userGameRepository.delete(userGame));
    }
    @GetMapping("/getCurrentPlayer")
    public User getCurrentUser(){
        Authentication loggedInUser = SecurityContextHolder.getContext().getAuthentication();
        String email = loggedInUser.getName();
        User user = userRepository.findByEmail(email);
        return user;

    }
    @GetMapping("/gamesuggestlist/getGames")
    public List<GameSuggestion> list() {
        return gameSuggestionRepository.findAll();
    }

    @GetMapping("/getGame/{game_id}")
    public Optional<Game> getGame(@PathVariable("game_id") long game_id){
        return gameRepository.findById(game_id);
    }

    @GetMapping("/add/getNewGame")
    public Game getGame(){
        return new Game();
    }

    @PostMapping("/add/setNewGame")
    public void setNewGame(@RequestBody Game game){
        Game newGame = new Game(game.getName(),
                game.getDescription(), game.getNum_players(), game.getDuration(), game.getCategory());
        gameRepository.save(newGame);
    }
    @PostMapping("/gamesuggestlist/setGame")
    public void setNewSuggestedGame(@RequestBody GameSuggestion game){
        Game newGame = new Game(game.getName(),
                game.getDescription(), game.getNum_players(), game.getDuration(), game.getCategory());
        gameRepository.save(newGame);
        gameSuggestionRepository.deleteById(game.getSuggestion_id());
    }
    @PostMapping("/gamesuggestlist/removeGame")
    public void removeSuggestedGame(@RequestBody GameSuggestion game){
        gameSuggestionRepository.deleteById(game.getSuggestion_id());
    }

    @PostMapping("/add/setNewSuggestedGame")
    public void setNewSuggestedGame(@RequestBody Game game){
        GameSuggestion newGame = new GameSuggestion(game.getName(),
                game.getDescription(), game.getNum_players(), game.getDuration(), game.getCategory());
        gameSuggestionRepository.save(newGame);
    }
    //dodano za inventory
    @GetMapping("/inventory/getUsersInventory")
    public Set<UserGame> getUsersInventory(){
        Authentication loggedInUser = SecurityContextHolder.getContext().getAuthentication();
        String email = loggedInUser.getName();
        User user = userRepository.findByEmail(email);
        // return userGameRepository.findInventoryByUserId(user.getId());
        return userGameRepository.findUserGameInventory(user.getId());
//        return userRepository.findInventoryByUserId(user.getId());
    }

    @GetMapping("/userGame/{id}")
    public List<UserGame> getUsergame(@PathVariable("id") long id) {
        User user=userRepository.findById(id).orElse(null);
        Set<UserGame> userGame =userGameRepository.findUserGameInventory(user.getId()) ;
        return userGame.stream().filter(g-> g.isZaIznajmljivanje()).collect(Collectors.toList());

    }

    @GetMapping("/rentGames/getPlayers")
    public List<User> getPlayers() {
        Set<String> lista = userGameRepository.findPlayersWhoRentGames();
        List<User> listaUser = new ArrayList<>();
        lista.stream().forEach(g->{
            listaUser.add(userRepository.findByEmail(g));
        });
        return listaUser.stream().filter(u-> !u.isIznajmljivac()).collect(Collectors.toList());
    }
    @GetMapping("/rentGames/getCompany")
    public List<User> getCompany() {
        Set<String> lista = userGameRepository.findCompanyWhoRentGames();
        List<User> listaUser = new ArrayList<>();
        lista.stream().forEach(g->{
            listaUser.add(userRepository.findByEmail(g));
        });
        return listaUser.stream().filter(u-> u.isIznajmljivac()).collect(Collectors.toList());
    }

    @PostMapping("/myinventory/rentGame")
    public void rentGame(@RequestParam("0") Long id, @RequestBody UserGame userGame) {
        Authentication loggedInUser = SecurityContextHolder.getContext().getAuthentication();
        String email = loggedInUser.getName();
        User user = userRepository.findByEmail(email);
        userGame.setGameId(userGame.getGameId());
        userGame.setUserId(user);
        userGame.setPosudena(true);
        userGame.setZaIznajmljivanje(false);
        userGameRepository.save(userGame);
        user.addGameToInventory(userGame);
        userRepository.save(user);

        User userPosudenaOd = userRepository.findById(id).orElse(null);
        UserGame userp = new UserGame();
        userp.setUserId(userPosudenaOd);
        userp.setGameId(userGame.getGameId());
        userp.setPosudena(true);
        userp.setCijena(0);
        userp.setZaIznajmljivanje(false);
        userGameRepository.save(userp);
        userPosudenaOd.addGameToInventory(userp);
        userRepository.save(userPosudenaOd);
    }

    @GetMapping("/rentGames/getPlayerInventory")
    public Set<Game> getPlayerInventory(@RequestBody User user) {
        return userGameRepository.findInventoryByUserId(user.getId());
    }

    @PostMapping("/myinventory/removeFromRenting")
    public void removeFromRenting(@RequestBody Game game) {
        Authentication loggedInUser = SecurityContextHolder.getContext().getAuthentication();
        String email = loggedInUser.getName();
        User user = userRepository.findByEmail(email);
        UserGame userGame = userGameRepository.findInventoryByGameId(game.getId(), user.getId());
        userGame.setZaIznajmljivanje(false);
        userGame.setCijena(0);
        userGameRepository.save(userGame);
    }

    @PostMapping("/myinventory/setForRent")
    public void setGameForRent(@RequestParam("0") Float price, @RequestBody Game game) {
        Authentication loggedInUser = SecurityContextHolder.getContext().getAuthentication();
        String email = loggedInUser.getName();
        User user = userRepository.findByEmail(email);
        UserGame userGame = userGameRepository.findInventoryByGameId(game.getId(), user.getId());
        userGame.setZaIznajmljivanje(true);
        userGame.setCijena(price);
        userGameRepository.save(userGame);
    }
    @PostMapping("/myinventory/addGame")
    public void addGameToInventory(@RequestBody Game game){
        Authentication loggedInUser = SecurityContextHolder.getContext().getAuthentication();
        String email = loggedInUser.getName();
        User user = userRepository.findByEmail(email);
        UserGame userGame = new UserGame();
        userGame.setGameId(game);
        userGame.setUserId(user);
        userGameRepository.save(userGame);
        Set<UserGame> inventory=userGameRepository.findUserGameInventory(user.getId());
        user.addGameToInventory(userGame);
        userRepository.save(user);
    }

}

