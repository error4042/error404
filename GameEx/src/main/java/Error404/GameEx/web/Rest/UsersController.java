package Error404.GameEx.web.Rest;

import Error404.GameEx.models.Grupe;
import Error404.GameEx.models.Role;
import Error404.GameEx.models.User;
import Error404.GameEx.models.VerificationToken;
import Error404.GameEx.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Query;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/admin/users")
public class UsersController {

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final VerificationTokenRepository verificationTokenRepository;
    private final GrupeRepository grupeRepository;
    private final UserGameRepository userGameRepository;
    private final MessageRepository messageRepository;
    @Autowired
    private AdRepository adRepository;
    @Autowired
    private RecenzijaRepository recenzijaRepository;
    public UsersController(UserRepository userRepository,RoleRepository roleRepository,VerificationTokenRepository verificationTokenRepository,GrupeRepository grupeRepository,UserGameRepository userGameRepository,MessageRepository messageRepository) {
        this.grupeRepository=grupeRepository;
        this.userRepository = userRepository;
        this.roleRepository=roleRepository;
        this.verificationTokenRepository=verificationTokenRepository;
        this.userGameRepository=userGameRepository;
        this.messageRepository=messageRepository;
    }

    @GetMapping("/getUsers")
    public List<User> list() {
        return userRepository.findAll();
    }
    @GetMapping("/getUser/{id}")
    public Optional<User> getUser(@PathVariable("id") long id){
        return userRepository.findById(id);
    }
    @PostMapping("/setUser")
    public void setUser(@RequestBody User user){
        User oldUser=userRepository.findById(user.getId()).orElse(null);
        oldUser.setFirstName(user.getFirstName());
        oldUser.setLastName(user.getLastName());
        oldUser.setEmail(user.getEmail());
        oldUser.setProfilePicture(user.getProfilePicture());
        oldUser.setEnabled(user.isEnabled());
        if(oldUser.isIznajmljivac()) {
            oldUser.setTelephone(user.getTelephone());
            oldUser.setCompanyName(user.getCompanyName());
            oldUser.setWebAddress(user.getWebAddress());
            oldUser.setAddress(user.getAddress());
        }
        userRepository.save(oldUser);

    }
    @PostMapping("/deleteUser")
    public void deleteUser(@RequestBody User user){
        int i;
        User oldUser=userRepository.findById(user.getId()).orElse(null);
        VerificationToken token= verificationTokenRepository.findByUser(oldUser);
        oldUser.getRoles().clear();
        ArrayList<Grupe> grupe= new ArrayList<>();
        grupe.addAll(grupeRepository.findAll().stream().filter(grupe1 -> grupe1.hasMember(oldUser)).collect(Collectors.toList()));
        for(i=0;i<grupe.size();i++){
            grupe.get(i).removeUser(oldUser);
            grupeRepository.save(grupe.get(i));
        }
        adRepository.findAll().stream().filter(ad -> ad.getUser().equals(oldUser)).forEach(ad -> adRepository.delete(ad));
        userGameRepository.findUserGameInventory(user.getId()).forEach(userGame -> userGameRepository.delete(userGame));
        messageRepository.findMessagesFromUser(user.getId()).forEach(message -> message.setUser(null));

        recenzijaRepository.findAll().stream().filter(recenzija -> recenzija.getNapisaouser().equals(oldUser) || recenzija.getPrimiouser().equals(oldUser))
                .forEach(recenzija -> recenzijaRepository.delete(recenzija));
        verificationTokenRepository.delete(token);
    }
    @PostMapping("/setIznajmljivac")
    public void setIznajmljivac(@RequestBody User user){
        User OldUser=userRepository.findById(user.getId()).orElse(null);
        if(OldUser.isIznajmljivac()){
            OldUser.removeRole(roleRepository.findRoleByName("ROLE_IZNAJMLJIVAC"));
            OldUser.addRole(roleRepository.findRoleByName("ROLE_IGRAC"));
            OldUser.setIznajmljivac(false);
            userRepository.save(OldUser);
        }else{
            OldUser.addRole(roleRepository.findRoleByName("ROLE_IZNAJMLJIVAC"));
            OldUser.removeRole(roleRepository.findRoleByName("ROLE_IGRAC"));
            OldUser.removeRole(roleRepository.findRoleByName("ROLE_MODERATOR"));
            OldUser.setIznajmljivac(true);
            OldUser.setModerator(false);
            userRepository.save(OldUser);
        }


    }
    @PostMapping("/setModerator")
    public void setModerator(@RequestBody User user){
        User OldUser=userRepository.findById(user.getId()).orElse(null);
        if(OldUser.isModerator()) {
            OldUser.removeRole(roleRepository.findRoleByName("ROLE_MODERATOR"));
            if(!OldUser.getRoles().contains(roleRepository.findRoleByName("ROLE_IGRAC"))){
                OldUser.addRole(roleRepository.findRoleByName("ROLE_IGRAC"));
            }
            OldUser.setModerator(false);
            OldUser.setIznajmljivac(false);
            userRepository.save(OldUser);
        }else{
            OldUser.addRole(roleRepository.findRoleByName("ROLE_MODERATOR"));
            if(OldUser.isIznajmljivac()){
                OldUser.removeRole(roleRepository.findRoleByName("ROLE_IZNAJMLJIVAC"));
                OldUser.setIznajmljivac(false);
            }
            OldUser.setModerator(true);
            userRepository.save(OldUser);
        }




    }


}