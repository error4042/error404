package Error404.GameEx.web.Rest;

import Error404.GameEx.models.Role;
import Error404.GameEx.repository.RoleRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/admin/roles")
public class RolesController {

    private final RoleRepository roleRepository;

    public RolesController(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    @GetMapping("/getroles")
    public List<Role> list() {
        return roleRepository.findAll();
    }
    @GetMapping("/getRole/{id}")
    public Optional<Role> getRole(@PathVariable("id") long id){
        return roleRepository.findById(id);
    }
    @PostMapping("/setRole")
    public void setRole(@RequestBody Role role){
        Role oldRole=roleRepository.findById(role.getId()).orElse(null);
        oldRole.setName(role.getName());
        roleRepository.save(oldRole);

    }
    @GetMapping("/create/getNewRole")
    public Role getRole(){
        return new Role("ROLE_NEW");
    }

    @PostMapping("/create/setNewRole")
    public void setnewRole(@RequestBody Role role){
        Role newRole=new Role(role.getName());
        roleRepository.save(newRole);
    }
    @PostMapping("/deleteRole")
    public void deleteRole(@RequestBody Role role){
        roleRepository.delete(role);
    }



}