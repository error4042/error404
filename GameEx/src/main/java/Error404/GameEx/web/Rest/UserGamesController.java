package Error404.GameEx.web.Rest;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import Error404.GameEx.models.User;
import Error404.GameEx.models.UserGame;
import Error404.GameEx.repository.UserGameRepository;
import Error404.GameEx.repository.UserRepository;

@RestController
public class UserGamesController {

	private final UserRepository userRepository;
	private final UserGameRepository userGameRepostory;

	public UserGamesController(UserRepository userRepository, UserGameRepository userGameRepository) {
		super();
		this.userRepository = userRepository;
		this.userGameRepostory = userGameRepository;
	}


	@PostMapping("/myinventory/addGame")
	public void addGameToInventory(@RequestBody UserGame game){
		Authentication loggedInUser = SecurityContextHolder.getContext().getAuthentication();
		String email = loggedInUser.getName();
		User user = userRepository.findByEmail(email);
		//Game newgame=new Game();
		userRepository.save(user);
	}
}
