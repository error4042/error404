package Error404.GameEx.web.Rest;

import Error404.GameEx.models.Recenzija;
import Error404.GameEx.models.User;
import Error404.GameEx.repository.RecenzijaRepository;
import Error404.GameEx.repository.UserRepository;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/recenzije")
public class RecenzijeController {

    private final RecenzijaRepository recenzijaRepository;
    private final UserRepository userRepository;

    public RecenzijeController(RecenzijaRepository recenzijaRepository, UserRepository userRepository) {
        this.recenzijaRepository = recenzijaRepository;
        this.userRepository = userRepository;
    }

    @GetMapping("/createRecenzija/getCurrentUser")
    public User getCurrentUser(){
        Authentication loggedInUser = SecurityContextHolder.getContext().getAuthentication();
        String email = loggedInUser.getName();
        User user = userRepository.findByEmail(email);
        return user;
    }

    @GetMapping("/createNewRecenzija")
    public Recenzija newRecenzija() {
        return new Recenzija();
    }

    @GetMapping("/getRecenzije")
    public List<Recenzija> list() {
        return recenzijaRepository.findAll();
    }

    @PostMapping("/setRecenzija")
    public void setRecenzija(@RequestBody Recenzija recenzija){
        Authentication loggedInUser = SecurityContextHolder.getContext().getAuthentication();
        String email = loggedInUser.getName();
        User user = userRepository.findByEmail(email);

        Recenzija rec = new Recenzija(recenzija.getKomentar(), recenzija.getOcjena(),
                recenzija.getNapisaouser(), recenzija.getPrimiouser());
        recenzijaRepository.save(rec);
    }

    @PostMapping("/deleteRecenzija/{id}")
    public void deleteRecenzija(@PathVariable("id") long id) {
        Recenzija recenzijaDelete = recenzijaRepository.findById(id).orElse(null);
        recenzijaRepository.delete(recenzijaDelete);

    }
    @GetMapping("/getUser/{id}")
    public User getUser(@PathVariable("id") long id){
        return userRepository.findById(id).orElse(null);
    }
}
