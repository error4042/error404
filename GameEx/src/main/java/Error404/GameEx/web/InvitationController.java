package Error404.GameEx.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@Controller
public class InvitationController {

    @GetMapping("/createInvitation/{group_id}")
    public String createInvitation(){
        return "Invitation/createInvitation";
    }
    @GetMapping("/myInvitations")
    public String myInvitation(){
        return "Invitation/myInvitations";
    }

}
