package Error404.GameEx.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MainController {
    @GetMapping("/login")
    public String login(){
        return "login";
    }

    @GetMapping("/")
    public String home(){
        return "index";
    }

    @GetMapping("/admin")
    public String admin(){
        return "admin";
    }

    @GetMapping("/createads")
    public String createads(){ return "createads"; }

    @GetMapping("/userslist")
    public String userslist(){ return "userslist"; }
    @GetMapping("/userslist/inventory/{id}")
    public String iznajmljivacInventory(){
        return "iznajmljivacRentedGames";
    }
    @GetMapping("/profile")
    public String profile(){
        return "Profile/Profile";
    }
}
