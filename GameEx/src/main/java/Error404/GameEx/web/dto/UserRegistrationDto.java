package Error404.GameEx.web.dto;

import javax.persistence.Column;

public class UserRegistrationDto {
    private String firstName;
    private String lastName;
    private String email;
    private String password;
    public String profilePicture;

    private String telephone;

    private String CompanyName;

    private String WebAddress;

    private String Address;

    private String OIB;

    public UserRegistrationDto(){

    }
    public UserRegistrationDto(String firstName, String lastName, String email, String password,String telephone,String companyname,String webaddress,String adress,String OIB) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
        this.telephone=telephone;
        this.CompanyName=companyname;
        this.WebAddress=webaddress;
        this.Address=adress;
        this.OIB=OIB;
    }
    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }
    public String getProfilePicture() {
        return profilePicture;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getCompanyName() {
        return CompanyName;
    }

    public void setCompanyName(String companyName) {
        CompanyName = companyName;
    }

    public String getWebAddress() {
        return WebAddress;
    }

    public void setWebAddress(String webAddress) {
        WebAddress = webAddress;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getOIB() {
        return OIB;
    }

    public void setOIB(String OIB) {
        this.OIB = OIB;
    }

}
