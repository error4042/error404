package Error404.GameEx.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
public class Message {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    @ManyToOne(fetch=FetchType.EAGER)
    private User user;

    @JsonIgnore
    @ManyToOne(cascade=CascadeType.ALL)
    private Forum forum;

    public Forum getForum() {
        return forum;
    }

    public void setForum(Forum forum) {
        this.forum = forum;
    }

    private String text;

    public Message(){

    }

    public Message(User user,String text) {
        this.user = user;
        this.text = text;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getText() {
        return text;
    }

    public Long getId() {
		return id;
	}

	public void setText(String text) {
        this.text = text;
    }
}
