package Error404.GameEx.models;

import javax.persistence.*;

@Entity
public class Invitation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long invitation_id;

    @Column(name = "description")
    private String description;

    @Column(name = "user_id")
    private long user_id;

    @Column(name="grupa_id")
    private long grupa_id;

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    private String groupName;

    public Invitation(){

    }
    public Invitation(String description, long user_id, long grupa_id, String groupName) {
        this.description = description;
        this.user_id = user_id;
        this.grupa_id = grupa_id;
        this.groupName=groupName;
    }

    public Long getInvitation_id() {
        return invitation_id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getUser_id() {
        return user_id;
    }

    public void setUser_id(long user_id) {
        this.user_id = user_id;
    }

    public long getGrupa_id() {
        return grupa_id;
    }

    public void setGrupa_id(long grupa_id) {
        this.grupa_id = grupa_id;
    }
}
