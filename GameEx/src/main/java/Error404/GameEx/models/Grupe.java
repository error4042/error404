package Error404.GameEx.models;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

@Entity
@Table(name="grupe")
public class Grupe {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @Column(name="Grupe_admin")
    private Long GrupeAdminId;

    private String game_name;

    @JsonFormat(pattern="yyyy-MM-dd", timezone="Europe/Zagreb")
    @Column(name="date_suggestion")
    private Date dateSuggestion;


    @ManyToMany
    @JoinTable(
            name="grupe_users",
            joinColumns = @JoinColumn(
                    name="grupe_id",referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(
                    name="user_id",referencedColumnName = "id"
            ),uniqueConstraints = @UniqueConstraint(columnNames = {"grupe_id", "user_id"})
    )
    private Collection<User> users;

    public String getGame_name() {
        return game_name;
    }

    public void setGame_name(String game_name) {
        this.game_name = game_name;
    }

    public Grupe(){

    }
    public Grupe(String name,long userId,Collection<User> users,String game_name){
        this.name=name;
        this.GrupeAdminId = userId;
        this.users=users;
        this.game_name=game_name;
    }
    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Collection<User> getUsers() {
        return this.users;
    }

    public void setUsers(Collection<User> users) {
        this.users = users;
    }
    public Long getGroupAdminId() {
        return GrupeAdminId;
    }
    public void addUser(User user){
        this.users.add(user);
    }
    public void setGroupAdminId(Long groupAdminId){
        GrupeAdminId = groupAdminId;
    }
    public boolean hasMember(User user){
        return users.contains(user);

    }
    public void removeUser(User user){
        users.remove(user);
    }

    //dodano

    public Date getDateSuggestion() {
        return dateSuggestion;
    }

    public void setDateSuggestion(Date dateSuggestion) {
        this.dateSuggestion = dateSuggestion;
    }
}
