package Error404.GameEx.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "users_games")
@IdClass(UserGameId.class)
public class UserGame {

	public  UserGame() {
		this.zaIznajmljivanje=false;
	}

	@Id
	@ManyToOne
	@JsonIgnore
	@JoinColumn(name = "user_id", referencedColumnName = "id")
	private User userId;
	
	@Id
	@ManyToOne
	@JoinColumn(name = "game_id", referencedColumnName = "game_id")
	private Game gameId;
	
	@Column(name = "za_iznajmljivanje")
	private boolean zaIznajmljivanje;

	private float cijena;

	private boolean posudena;


	public User getUserId() {
		return userId;
	}
	public void setUserId(User userId) {
		this.userId = userId;
	}
	public Game getGameId() {
		return gameId;
	}
	public void setGameId(Game gameId) {
		this.gameId = gameId;
	}
	public boolean isZaIznajmljivanje() {
		return zaIznajmljivanje;
	}
	public void setZaIznajmljivanje(boolean zaIznajmljivanje) {
		this.zaIznajmljivanje = zaIznajmljivanje;
	}

	public boolean isPosudena() {
		return posudena;
	}

	public void setPosudena(boolean posudena) {
		this.posudena = posudena;
	}

	public float getCijena() {
		return cijena;
	}

	public void setCijena(float cijena) {
		this.cijena = cijena;
	}
}
