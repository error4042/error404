package Error404.GameEx.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Collection;
import java.util.List;

@Entity
public class Forum {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    @OneToOne(fetch=FetchType.EAGER,cascade=CascadeType.ALL)
    private Grupe grupa;

    @OneToMany(mappedBy="forum", cascade=CascadeType.ALL)
    private List<Message> messages;

    public List<Message> getMessages() {
        return messages;
    }

    public Forum(){

    }
    public Forum(Grupe grupa) {
        this.grupa = grupa;
    }


    public Grupe getGrupa() {
        return grupa;
    }

    public void setGrupa(Grupe grupa) {
        this.grupa = grupa;
    }
}
