package Error404.GameEx.models;

import javax.persistence.*;
import java.util.Collection;
import java.util.Set;

@Entity
@Table(name="game", uniqueConstraints = @UniqueConstraint(columnNames="game_id"))
public class Game {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long game_id;

    @Column(name = "name")
    private String name;
    @Column(name = "description")
    private String description;
    @Column(name = "num_players")
    private Integer num_players;
    @Column(name = "duration")
    private String duration;

    @Column(name = "category")
    private String category;

//    @ManyToOne
//    private Category category;

    @OneToMany(mappedBy = "userId")
    private Set<UserGame> inventory;

    public Game() { }

    public Game(String name,String description, Integer num_players, String duration, String category) {
        this.name = name;
        this.description = description;
        this.num_players = num_players;
        this.duration = duration;
        this.category = category;
    }

    public Long getId() { return game_id; }

    public void setId(Long game_id) {
        this.game_id = game_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getNum_players() {
        return num_players;
    }

    public void setNum_players(Integer num_players) {
        this.num_players = num_players;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getCategory() { return category; }

    public void setCategory(String category) { this.category = category; }
}

