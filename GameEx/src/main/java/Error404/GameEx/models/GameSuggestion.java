package Error404.GameEx.models;

import javax.persistence.*;

@Entity
public class GameSuggestion {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long suggestion_id;

    @Column(name = "name")
    private String name;
    @Column(name = "description")
    private String description;

    public Long getSuggestion_id() {
        return suggestion_id;
    }

    @Column(name = "num_players")
    private Integer num_players;
    @Column(name = "duration")
    private String duration;

    @Column(name = "category")
    private String category;

    public GameSuggestion() {

    }
    public GameSuggestion(String name, String description, Integer num_players, String duration, String category) {
        this.name = name;
        this.description = description;
        this.num_players = num_players;
        this.duration = duration;
        this.category = category;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getNum_players() {
        return num_players;
    }

    public void setNum_players(Integer num_players) {
        this.num_players = num_players;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}
