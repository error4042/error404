package Error404.GameEx.models;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.lang.reflect.Array;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

@Entity
@Table(name = "ad")
public class Ad {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long ad_id;

    @Column(name = "description")
    private String description;

    @JsonFormat(pattern="yyyy-MM-dd", timezone="Europe/Zagreb")
    @Column(name = "date")
    private Date date;

    @Column(name = "location")
    private String location;

    @Column(name="time")
    private String time;

    @OneToOne
    private User user;

    @ManyToOne
    private Game game;

    public Ad(){

    }

    public Ad(String description, Date date, String location, User user, Game game,String time){
        this.description = description;
        this.date = date;
        this.location = location;
        this.user = user;
        this.game = game;
        this.time=time;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Long getAd_id() {
        return ad_id;
    }

    public void setAd_id(Long ad_id) {
        this.ad_id = ad_id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }
}
