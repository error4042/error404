package Error404.GameEx.models;

import javax.persistence.*;
import java.util.Collection;

@Entity
public class Recenzija {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String komentar;
    private int ocjena;
    //private Long userKomentar;

    @ManyToOne
    private User napisaouser;


    @ManyToOne
    private User primiouser;
    public Recenzija() {

    }

    public Recenzija(String komentar, int ocjena, User napisaouser,User primiouser){
        this.komentar = komentar;
        this.ocjena = ocjena;
        //  this.userKomentar = userKomentar;
        this.napisaouser = napisaouser;
        this.primiouser=primiouser;
    }

    public Long getId() {
        return id;
    }

    public User getNapisaouser() {
        return napisaouser;
    }

    public void setNapisaouser(User napisaouser) {
        this.napisaouser = napisaouser;
    }

    public User getPrimiouser() {
        return primiouser;
    }

    public void setPrimiouser(User primiouser) {
        this.primiouser = primiouser;
    }


    public void setId(Long id) {
        this.id = id;
    }

    public String getKomentar() {
        return komentar;
    }

    public void setKomentar(String komentar) {
        this.komentar = komentar;
    }

    public int getOcjena() {
        return ocjena;
    }

    public void setOcjena(int ocjena) {
        this.ocjena = ocjena;
    }

    // public Long getUserKomentar() {
    //      return userKomentar;
//}

    // public void setUserKomentar(Long userKomentar) {
    //  this.userKomentar = userKomentar;
    //  }
}
