package Error404.GameEx.models;

import java.io.Serializable;

public class UserGameId implements Serializable{

	private long userId;
	private long gameId;
	public long getUserId() {
		return userId;
		
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public long getGameId() {
		return gameId;
	}
	public void setGameId(long gameId) {
		this.gameId = gameId;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (gameId ^ (gameId >>> 32));
		result = prime * result + (int) (userId ^ (userId >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserGameId other = (UserGameId) obj;
		if (gameId != other.gameId)
			return false;
		if (userId != other.userId)
			return false;
		return true;
	}
	
	
}
