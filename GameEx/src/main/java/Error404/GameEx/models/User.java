package Error404.GameEx.models;

import org.springframework.data.jpa.repository.Query;
import org.springframework.web.bind.annotation.ModelAttribute;

import javax.persistence.*;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Table(name="user", uniqueConstraints = @UniqueConstraint(columnNames="email"))
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name="first_name")
    private String firstName;
    @Column(name="last_name")
    private String lastName;
    private String email;
    @Column(nullable = true, length = 64)
    private String profilePicture;
    private String password;
    private boolean enabled;
    @Column(nullable = true)
    private String telephone;
    @Column(name="company_name",nullable = true)
    private String CompanyName;
    @Column(name="web_address",nullable = true)
    private String WebAddress;

    @Column(nullable = true)
    private String Address;
    @Column(nullable = true)
    private String OIB;
    private boolean isIznajmljivac;
    private boolean isModerator;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "gameId")
    private Collection<UserGame> inventory;
    
    //ManyToMany user - games, za inventory
//    @ManyToMany(fetch=FetchType.EAGER,cascade=CascadeType.ALL)
//    @JoinTable(
//            name="users_games",
//            joinColumns = @JoinColumn(
//                    name="user_id",referencedColumnName = "id"),
//            inverseJoinColumns = @JoinColumn(
//                    name="game_id",referencedColumnName = "game_id"), uniqueConstraints = @UniqueConstraint(columnNames = {"user_id", "game_id"})
//    )
//    private Set<Game> inventory;

    @ManyToMany(fetch=FetchType.EAGER,cascade=CascadeType.ALL)
    @JoinTable(
            name="users_roles",
            joinColumns = @JoinColumn(
                    name="user_id",referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(
                    name="role_id",referencedColumnName = "id"
            ),uniqueConstraints = @UniqueConstraint(columnNames = {"user_id", "role_id"})
    )
    private Collection<Role> roles;

    public User(){

    }
    public User(String firstName, String lastName, String email,String profilePicture, String password, Collection<Error404.GameEx.models.Role> roles,
                Set<Game> inventory) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
        this.roles = roles;
        this.profilePicture=profilePicture;
//        this.inventory = inventory;
        this.isModerator=false;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Collection<Error404.GameEx.models.Role> getRoles() {
        return roles;
    }
    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getCompanyName() {
        return CompanyName;
    }

    public void setCompanyName(String companyName) {
        CompanyName = companyName;
    }

    public String getWebAddress() {
        return WebAddress;
    }

    public void setWebAddress(String webAddress) {
        WebAddress = webAddress;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getOIB() {
        return OIB;
    }

    public void setOIB(String OIB) {
        this.OIB = OIB;
    }

    public boolean isIznajmljivac() {
        return isIznajmljivac;
    }

    public boolean isModerator() {
        return isModerator;
    }

    public void setModerator(boolean moderator) {
        isModerator = moderator;
    }
//
    public Collection<UserGame> getInventory() { return inventory; }

    public void setInventory(Set<UserGame> inventory) { this.inventory = inventory; }

    public void setIznajmljivac(boolean iznajmljivac) {
        isIznajmljivac = iznajmljivac;
    }
    public void setRoles(Collection<Error404.GameEx.models.Role> roles) {
        this.roles = roles;
    }
    public void removeRole(Role role){
        roles.remove(role);
    }
    public void addRole(Role role){
        roles.add(role);
    }
    public boolean isEnabled() {
        return enabled;
    }
    public String getProfilePicture() {
        return profilePicture;
    }
    public void addGameToInventory(UserGame game){
        this.inventory.add(game);
    }
    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
    @Transient
    public String getPhotosImagePath() {
        if (profilePicture == null || id == null) return null;

        return "/user-photos/" + id + "/" + profilePicture;
    }

    public void removeGameFromInventory(UserGame game){
        this.inventory.remove(game);
    }
    //public boolean containsGameInInventoryById(Game game){
   //      List<UserGame> list=this.inventory.stream().filter(userGame -> userGame.getGameId().equals(game)).collect(Collectors.toList());
   //      if(list.isEmpty()){
  //           return false;
  //       }else{
  //           return true;
  //       }
  //  }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id.equals(user.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
