package Error404.GameEx.models;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="category", uniqueConstraints = @UniqueConstraint(columnNames="id"))
public class Category {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name="name")
    private String name;

    @OneToMany(fetch=FetchType.EAGER,cascade=CascadeType.ALL)
    @JoinColumn(name = "category_id")
    private List<Game> games = new ArrayList<>();


    public Category() { }
    public Category(String name) { this.name = name; }

    public Long getId() { return id; }

    public void setId(Long id) { this.id = id; }

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    public List<Game> getGames() { return games; }

    public void setGames(List<Game> games) { this.games = games; }
}
