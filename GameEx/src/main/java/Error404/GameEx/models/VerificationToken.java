package Error404.GameEx.models;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name="verification_token")
public class VerificationToken {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String token;
    @Column(name="expiry_date")
    private Timestamp expiryDate;
    @OneToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name="user_id",referencedColumnName = "id")
    private User user;
    public VerificationToken(){

    }

    public VerificationToken(String token, User user) {
        this.token = token;
        this.user = user;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Timestamp getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Timestamp expiryDate) {
        this.expiryDate = expiryDate;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
    public void removeUser(){
       this.user=null;
    }
}
