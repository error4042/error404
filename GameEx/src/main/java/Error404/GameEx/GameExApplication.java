package Error404.GameEx;

import Error404.GameEx.models.Category;
import Error404.GameEx.models.Game;
import Error404.GameEx.models.Role;
import Error404.GameEx.models.User;
import Error404.GameEx.repository.GameRepository;
import Error404.GameEx.repository.RoleRepository;
import Error404.GameEx.repository.UserRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.nio.file.attribute.UserDefinedFileAttributeView;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@SpringBootApplication
public class GameExApplication {

	public static void main(String[] args) {
		SpringApplication.run(GameExApplication.class, args);
	}

	@Bean
	CommandLineRunner init (RoleRepository roles) {
		return args -> {
			if(!roles.existsById((long) 1)) {
				Role role1 = new Role("ROLE_IGRAC");
				Role role2 = new Role("ROLE_IZNAJMLJIVAC");
				Role role3 = new Role("ROLE_MODERATOR");
				Role role4 = new Role("ROLE_ADMIN");
				List<Role> list = Arrays.asList(role1, role2, role3, role4);
				list.forEach(role -> roles.save(role));
			}
		};
	}

}
