package Error404.GameEx.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import Error404.GameEx.models.Game;
import Error404.GameEx.models.User;
import Error404.GameEx.service.UserServiceImpl;
import Error404.GameEx.web.Rest.UsersController;
@SpringBootTest
public class UserRepositoryTest {

	@Autowired
	UserServiceImpl userService;
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	private UsersController uc;
	
	@Test
	public void findUserByEmailTest() {
		Optional<User> user = uc.getUser(1L); 
		String email = user.get().getEmail(); 
		User foundUser = userRepository.findByEmail(email); 
		assertEquals(user.get(), foundUser); 
	}
	
}
