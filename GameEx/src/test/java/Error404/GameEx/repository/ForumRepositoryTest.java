package Error404.GameEx.repository;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import Error404.GameEx.models.Forum;
import Error404.GameEx.models.Grupe;
import Error404.GameEx.web.Rest.GrupeController;

@SpringBootTest
public class ForumRepositoryTest {
	
	
	@Autowired
	ForumRepository forumRepository;
	
	@Autowired
	private GrupeController gc;
	
	@Test
	public void findForumByGroupIdTest() {
		List<Grupe> grupe = gc.list(); 
		Forum forum = forumRepository.findForumByGroupId(grupe.get(0).getId()); 
		assertNotNull(forum);
	}
	

}
