package Error404.GameEx.web;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class UserControllerTest {
	@Autowired
	private UserController uc;
	
	@Test
	public void contextLoads() throws Exception{
		assertNotNull(uc);
	}

	@Test
	public void usersShouldReturnUsersRedirect() throws Exception {
		assertEquals("users", uc.users()); 
	}
	
	@Test
	public void editShouldReturnEditRedirect() throws Exception {
		assertEquals("editUsers", uc.edit()); 
	}
}
