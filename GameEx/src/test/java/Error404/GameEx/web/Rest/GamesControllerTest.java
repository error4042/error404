package Error404.GameEx.web.Rest;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class GamesControllerTest {
	@Autowired
	private GamesController gc;
	
	@Test
	public void contextLoads() throws Exception{
		assertNotNull(gc);
	}

	@Test
	public void gamesShouldBeListed() throws Exception {
		assertNotNull(gc.listSuggestion());
	}
	
	@Test
	public void findGameByIdTest() throws Exception {
		assertNotNull(gc.getGame(1L)); //u bazi postoji igra s id 1L
	}
	
}
