package Error404.GameEx.web.Rest;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;


@SpringBootTest
public class ForumsControllerTest {
	@Autowired
	private ForumsController fc;
	
	@Test
	public void contextLoads() throws Exception{
		assertNotNull(fc);
	}

	@Test
	public void messageShouldBeReturned() throws Exception {
		assertNotNull(fc.getEmptyMessage()); //uspjesno dohvaca praznu poruku
	}
	
	@Test
	public void messagesShouldBeReturned() throws Exception {
		assertNotNull(fc.getMessages(1L)); 
		try {
			fc.getMessages(2L);
			Assertions.fail("");
		}catch(NullPointerException e) {
			
		}
	}
	
}
