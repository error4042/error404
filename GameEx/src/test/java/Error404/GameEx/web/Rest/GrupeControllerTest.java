package Error404.GameEx.web.Rest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import Error404.GameEx.models.Grupe;

@SpringBootTest
public class GrupeControllerTest {
	@Autowired
	private GrupeController gc;
	
	@Test
	public void contextLoads() throws Exception{
		assertNotNull(gc);
	}
	
	@Test
	public void grupeShouldBeListed() throws Exception {
		List<Grupe> grupe = gc.listgrupe();
		assertNotNull(grupe);
	}
	@Test
	public void findUserByUsernameTest() throws Exception {
		assertEquals("Ime grupe", gc.getGrupa(1L).get().getName());
	}
}
