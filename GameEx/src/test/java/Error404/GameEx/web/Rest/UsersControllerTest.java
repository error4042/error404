package Error404.GameEx.web.Rest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import Error404.GameEx.models.User;

@SpringBootTest
public class UsersControllerTest{

	@Autowired
	private UsersController uc;
	
	@Test
	public void contextLoads() throws Exception{
		assertNotNull(uc);
	}

	@Test
	public void userShouldBeReturned() throws Exception {
		Optional<User> user = uc.getUser(1L); 
		assertEquals(1L, user.get().getId().longValue()); 
	}
	
}
